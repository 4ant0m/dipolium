var path = require ('path');
var util = require ('util');
var http = require ('http');
var crypto = require('crypto');
var async = require('async');
var HttpError = require('../error').HttpError;

var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema;
    var self = this;

var schema = new Schema({
    name: {
        type:String,
        unique:false,
        required:true
    },
    surname: {
        type:String,
        unique:false,
        required:true
    },
    mail:{
        type:String,
        unique:true,///!!!!!
        required:true,
        default: 'mail12@gmail.com'
    },
    about:{
        type:String,
        unique:false,
        required:false
    },
    ava:{
        type:String,
        unique:false,
        require:false,
        default:'undefined.jpg'
    },
    nickname: {
        type: String,
        unique: true,
        required: true
    },
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    lists: {
        type: Array,
        markModified:true,
        default: [{  "listname" : "Watch later",  "films" : [ ] },   {   "listname" : "Already watched",  "films" : [ ] } ]
    },
    followers:{
        type:Array,
        markModified:true,
        default: new Array()
    },
    following:{
        type:Array,
        markModified:true,
        default: new Array()
    },
    records:{
        type: Array,
        markModified: true,
        default: new Array()
    },
    count_followers: {
        type: Number,
        markModified: true,
        default : 0
    }

});

schema.methods.encryptPassword = function(password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.virtual('password')
    .set(function(password) {
        this._plainPassword = password;
        this.salt = Math.random() + '';
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function() { return this._plainPassword; });

schema.methods.checkPassword = function(password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

schema.methods.addRecord = function (Subject, Info) {
    var record = new Object();
        record.date = Date.now();
        record.Subject = Subject;
        record.Info = Info;
    this.markModified('records');
    this.records.push(record);
};
schema.methods.deleteRecord = function (indexRecord){
    this.markModified('records');
    this.records.splice(indexRecord,1);
    this.save();
};
schema.methods.addFollowing = function (id){
    var founded = false;
    this.markModified('following');
    for(var i = 0; i < this.following.length; i++){
        if (this.following[i].id === id){
            founded = true;
            break;
        }
    }
    if(founded === false){
    var following = new Object();
    	following.id = id;
    	following.indexRecord = this.records.length;
    this.following.push(following);
    this.save();
    this.addRecord(id,'AddFollowing');
}
    return this.following;
}

schema.methods.removeFollowing = function(id){
    var founded = false;
    this.markModified('following');
    for(var i = 0; i < this.following.length; i++){
        if (this.following[i].id === id || this.following[i] === id ){
            founded = true;
            break;
        }
    }
    if (founded === true){
    this.deleteRecord(this.following[i].indexRecord);
    this.following.splice(i,1);
    this.save();
}
    return this.following;
}

schema.methods.addFollowers = function(id){
    var founded = false;
    this.markModified('followers');
    this.markModified('count_followers');
    for (var i = 0; i < this.followers.length; i++){
        if (this.followers[i] === id){
            founded = true;
            break;
        }
    }
    if (founded === false)
    this.followers.push(id);
    this.count_followers = this.followers.length;
    this.save();
    return this.followers;
}
schema.methods.addLike = function(listID, filmID, idUser){
    this.markModified('lists');
    for (var i = 0; i < this.lists[listID].films.length; i++)
        if (this.lists[listID].films[i].filmID == filmID || this.lists[listID].films[i] == filmID){
            if (this.lists[listID].films[i].likes == undefined)
                this.lists[listID].films[i].likes = new Array();
                for (var j = 0; j < this.lists[listID].films[i].likes.length; j++)
                    if (this.lists[listID].films[i].likes[j] == idUser){
                        return
                    }
                    this.lists[listID].films[i].likes.push(idUser);
                    this.save();
                    console.log(this.lists[listID].films[i]);
                    break;
    }
}
schema.methods.removeLike = function (idList, idFilm, idUser){
    

}
schema.methods.removeFollowers = function(id){
    var founded = false;
    this.markModified('followers');
    this.markModified('count_followers');
    for (var i = 0; i < this.followers.length; i++){
        if (this.followers[i] ===id){
            founded = true;
            break;
        }
    }
    if (founded === true)
    this.followers.splice(i,1);
    this.count_followers = this.followers.length;
    this.save();
    return this.followers;
}

schema.methods.addList = function(list){
    this.lists[this.lists.length] = list;
    this.markModified('lists'); // указывает на то что свойство может быть изменено
    this.save();
    return this.lists;
};

schema.methods.removeList = function(index) {
    console.log('index - '+index+' //schema.methods.removeList');
    this.markModified('lists');
    this.lists.splice(index,1);
    this.save();
    return this.lists;
};

schema.methods.renameList = function (index, title) {
    console.log('index - '+index+' //schema.methods.removeList');
    this.markModified('lists');
    this.lists[index].listname = title;
    this.save();
    return this.lists;
};

schema.methods.changeAva = function (ava) {
    this.markModified('ava');
    this.ava = ava;
    this.save();
    return this.ava;
};

schema.methods.changeAbout = function (about) {
    this.markModified('about');
    this.about = about;
    this.save();
    return this.about;
};

schema.methods.changeMail = function (mail) {
    this.markModified('mail');
    this.mail = mail;
    this.save();
    return this.mail;
};

schema.methods.changeName = function (name) {
    this.markModified('name');
    this.name = name;
    this.save();
    return this.name;
};

schema.methods.changeSurname = function (surname) {
    this.markModified('surname');
    this.surname = surname;
    this.save();
    return this.surname;
};
schema.methods.changePassword = function (pass, newpass, confirmpass){
     this.markModified('password');
     if (this.checkPassword(pass) && newpass == confirmpass){
        this.password = newpass;
        this.save();
     }
    return this.password;
}


schema.methods.getListCount = function() {
    return this.lists.length;
}

schema.methods.addFilm = function(listID, filmID, comment){
    this.markModified('lists');
    var subject = {
            idList :listID,
            idFilm :filmID,
            comment :comment
        }
    //this.lists[listID].films=new Array();// указывает на то что свойство может быть изменено
    var founded = false;
    for(var i = 0; i < this.lists[listID].films.length; i++)
        if(this.lists[listID].films[i].filmID == filmID || this.lists[listID].films[i] == filmID ){
           founded = true;
            var film = new Object();
                film.filmID = filmID;
                film.indexRecord = this.records.length;
                if(comment != undefined)
                    film.comment = comment;
                else
                    film.comment = '';
            this.deleteRecord(this.lists[listID].films[i].indexRecord);
            this.lists[listID].films.splice(i,1);
            this.lists[listID].films.push(film);
            this.save();
            this.addRecord(subject, 'AddFilm');
            break;
        }
    if(founded == false){
        var film = new Object();
        film.filmID = filmID;
        film.indexRecord = this.records.length;
        if(comment != undefined)
            film.comment = comment;
        else
            film.comment = '';
    this.lists[listID].films.push(film);
    this.save();
    this.addRecord(subject, 'AddFilm');
    }
    return this.lists;
};

schema.methods.removeFilm = function(listID,filmID){
    this.markModified('lists');
    for(var i = 0; i < this.lists[listID].films.length; i++)
    if(this.lists[listID].films[i].filmID == filmID || this.lists[listID].films[i] == filmID){
    	this.deleteRecord(this.lists[listID].films[i].indexRecord);
        this.lists[listID].films.splice(i,1);
        break;
    }
    this.save();
    return this.lists;
};


schema.statics.authorize= function(email, name, surname, password, nickname, callback){
    console.log(nickname,name,surname,password, email);
    var User = this;
    async.waterfall([
        function(callback){
            console.log(email)
            User.findOne({mail:email},callback);
        }, function(user,callback){
            console.log(user);
            if(user){
                if(user.checkPassword(password)){
                    callback(null,user);
                }else{
                    callback(new AuthError("Login or password are wrong!"));
                }
            }else{

                if(name != undefined && surname != undefined && nickname != undefined){
                    nickname = nickname.toLowerCase();
                    var user = new User({nickname:nickname, name:name, surname:surname, password:password, mail:email});
                    user.save(function(err){
                        if (err) return callback(err);
                        callback(null,user);
                    });
            }else
                callback(new AuthError("Login or password are wrong!"));

            }

        }
    ], callback);
};
exports.User = mongoose.model('User', schema);



//ошибки для выдачи посетителю
function AuthError(status,message){
    Error.apply(this, arguments);
    Error.captureStackTrace(this, HttpError);


    this.message = message;
};

util.inherits(AuthError,Error);

AuthError.prototype.name = 'AuthError';

exports.AuthError = AuthError;



