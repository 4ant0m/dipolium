/**
 * Created by user on 17.08.14.
 */
var path = require ('path');
var util = require ('util');
var http = require ('http');
var crypto = require('crypto');
var async = require('async');
var HttpError = require('../error').HttpError;

var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    Title:{
        type:String,
        unique:false,
        required:true 
    }, 
    Year:{
        type:String,
        unique:false,
        required:true
    },
    Rated:{
        type:String,
        unique:false,
        required:true
    },
    Released:{
        type:String,
        unique:false,
        required:true
    }, 
    Runtime:{
        type:String,
        unique:false,
        required:true
    },
    Genre:{
        type:String,
        unique:false,
        required:true
    }, 
    Director:{
        type:String,
        unique:false,
        required:true
    },
    Writer:{
        type:String,
        unique:false,
        required:true
    },
    Actors:{
        type:String,
        unique:false,
        required:true
    },
    Plot:{
        type:String,
        unique:false,
        required:true
    },
    Language:{
        type:String,
        unique:false,
        required:true
    },
    Country:{
        type:String,
        unique:false,
        required:true
    },
    Awards:{
        type:String,
        unique:false,
        required:true
    },
    Poster:{
        type:String,
        unique:false,
        required:true
    },
    PosterOur:{
        type:String,
        unique:false,
        required:true
    },
    Metascore:{
        type:String,
        unique:false,
        required:true
    },
    imdbRating:{
        type:String,
        unique:false,
        required:true
    },
    imdbVotes:{
        type:String,
        unique:false,
        required:true
    },
    imdbID:{
        type:String,
        unique:false,
        required:true
    },
    Type:{
        type:String,
        unique:false,
        required:true
    },
    Response:{
        type:String,
        unique:false,
        required:true
    }
});

schema.statics.new= function(Title, Year, Rated, Released, Runtime, Genre, Director,Writer,Actors, Plot,Language,Country,Awards,Poster,PosterOur,Metascore,imdbRating,imdbVotes,imdbID,Type,Response){
    var Film= this;
    var film = new Film({Title:Title, Year: Year, Rated: Rated, Released:Released, Runtime: Runtime, Genre:Genre, Director:Director, Writer:Writer, Actors:Actors, Plot:Plot, Language:Language, Country:Country, Awards: Awards, Poster:Poster,PosterOur:PosterOur,Metascore:Metascore, imdbRating:imdbRating,imdbVotes:imdbVotes,imdbID:imdbID,Type:Type,Response:Response});
film.save(function(err){
});
    console.log('film '+Title+' saved');
}


exports.Film = mongoose.model('Film', schema);