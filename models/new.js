/**
 * Created by user on 1/22/15.
 */
/**
 * Created by user on 17.08.14.
 */
var path = require ('path');
var util = require ('util');
var http = require ('http');
var async = require('async');
var HttpError = require('../error').HttpError;

var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    Date:{
        type: Date,
        required: true,
        default: Date.now
    },
    Object:{
        type:String,
        required: false
    },
    Subject:{
        type:Object,
        required: false
    },
    Info:{
        type:String,
        required:true
    }
});
schema.statics.new = function(Object, Subject, Info){
    var New = this;
    var new_ = new New({Object:Object, Subject:Subject, Info:Info});
    new_.save(function(err){
    });
}

exports.New = mongoose.model('New', schema);