  /**
 * Module dependencies.
 */
var express = require('express');
var http = require('http');
var path = require('path');
var config = require('./config');
var log = require('./libs/log')(module);
var mongoose = require('./libs/mongoose');
var HttpError = require('./error').HttpError;
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var subdomain = require('express-subdomain');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var fs = require('fs');
var session = require('express-session');
var errorHandler = require('errorhandler');

var app = express();

// all environments
app.set('port', process.env.PORT || config.get('port'));
app.engine('ejs',require('ejs-locals'));
app.set('views', __dirname+ '/templates');
app.set('view engine', 'ejs');

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
}));

var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'})
 
app.use(morgan('combined', {stream: accessLogStream}))


app.use(cookieParser());

var MongoStore = require('connect-mongostore')(session);
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret : config.get('session:secret'),
    cookie: config.get('session:cookie'),
    key: config.get('session:key'),
    store: new MongoStore({'db': 'sessions'})
}));
app.use(require('./middleware/sendHttpError'));
app.use(require('./middleware/loadUser'));

//api specific routes 
//routes for mobile version
var routerMobile = express.Router();
require('./routesMobile')(routerMobile);
app.use(subdomain('m', routerMobile));

var router = express.Router();
require('./routes')(router);
app.use(router);


//static
app.use(express.static(path.join(__dirname, 'public')));
app.use(function(err,req,res,next){
      if(typeof err == 'number'){
              err= new HttpError(err);
      }
      if(err instanceof HttpError){
          res.sendHttpError(err);
      }else{

      if ('development' == app.get('env')) {
         errorHandler(err,req,res,next);
      }else{
      log.error(err);
      err = new HttpError(500);
      res.sendHttpError(err);
         }
      }

  });
http.createServer(app).listen(config.get('port'), function(){
  log.info('Express server listening on port ' + config.get('port'));
});
