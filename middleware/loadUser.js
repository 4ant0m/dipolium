var User = require('../models/user').User;

module.exports = function(req, res, next) {
    req.user = res.locals.user = null; //для того когда юзер не найден
    if (!req.session.user) return next();

    User.findById(req.session.user, function(err, user) {
        if (err) return next(err);

        req.user =res.locals.user = user; //res.locals - это то что будет переменная доступная для всех ejs'ок
        next();
    });
};