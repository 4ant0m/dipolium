/**
 * Created by user on 15.08.14.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;

module.exports = function(req, res, next) {
    var foundedUser = new Object();
    req.userFounded = res.locals.userFounded = null;
    User.findOne({nickname:req.params.nickname}, function(err,user) {
        if (err) return next(err);
        if(user){
            foundedUser.name = user.name;
            foundedUser.surname = user.surname;
            foundedUser.nickname = user.nickname;
            foundedUser.ava = user.ava;
            foundedUser.about = user.about;
            req.userFounded = res.locals.userFounded = foundedUser;
            next();
        } else {next(new HttpError(404, "Страница не найдена"));}
       // req.user =res.locals.user = user; //res.locals - это то что будет переменная доступная для всех ejs'ок

    });
};
 