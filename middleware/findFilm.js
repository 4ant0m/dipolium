
var Film = require('../models/film').Film;
var HttpError = require('../error').HttpError;

module.exports = function(req, res, next) {
    var foundedFilm = new Object();
    req.filmFounded = res.locals.filmFounded = null;
    Film.findOne({imdbID:req.params.idFilm}, function(err, film) {
        if (err) return next(err);
        if(film){
            foundedFilm.Title = film.Title;
            foundedFilm.Year = film.Year;
            foundedFilm.Rated = film.Rated;
            foundedFilm.Released = film.Released;
            foundedFilm.Runntime = film.Runntime;
            foundedFilm.Genre = film.Genre;
            foundedFilm.Director = film.Director;
            foundedFilm.Writer = film.Writer;
            foundedFilm.Actors = film.Actors;
            foundedFilm.Plot = film.Plot;
            foundedFilm.Language = film.Language;
            foundedFilm.Country = film.Country;
            foundedFilm.Awards = film.Awards;
            foundedFilm.PosterOur = film.PosterOur;
            foundedFilm.Metascore = film.Metascore;
            foundedFilm.imdbRating = film.imdbRating;
            foundedFilm.imdbVotes = film.imdbVotes;
            foundedFilm.Type = film.Type;
            foundedFilm.Responce = film.Responce;
            req.filmFounded = res.locals.filmFounded = foundedFilm;
            next();
        } else {next(new HttpError(404, "Page no found"));}
       // req.user =res.locals.user = user; //res.locals - это то что будет переменная доступная для всех ejs'ок

    });
};
 
