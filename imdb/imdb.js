/**
 * Created by user on 25/04/17.
 */
var request = require('request');
var api_key = '4deb64f4e35f23e516cd22b0ebe545d5';
var urlOmdb = 'http://www.omdbapi.com/';
//var urlIMDb='http://sg.media-imdb.com/suggests/'+name[0]+'/'+name+'.json';

var fs = require('fs'),
    request = require('request');

var download = function (uri, filename, callback) {
    //i++;
    try {
        request.head(uri, function (err, res, body) {
            request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
        });
    } catch (e) {
        callback(e);
        console.log(e.message + ' ');
        // if(i<10)
        //  download(uri, filename, callback,i);
    }
};

function generateImdbId(from, isIncrement, k) {
    var id = from,
        sign = (isIncrement) ? 1 : -1;
    k = k || 1;
    id += (k * sign);
    return id;
}


function getFilm(id, callback) {
    request(
        {
            url: urlOmdb + '?i=tt' + id + '&plot=full',
            encoding: null,
            headers: {
                'Accept': 'application/json'
            }
        },
        callback
    );
}

function addFilm(data) {
    //Film.new(data.Title, data.Year, data.Rated, data.Released, data.Runtime, data.Genre, data.Director, data.Writer, data.Actors, data.Plot, data.Language, data.Country, data.Awards, data.Poster, data.PosterOur, data.Metascore, data.imdbRating, data.imdbVotes, data.imdbID, data.Type, data.Response);
}

function findFilm(data, callback) {
    //Film.findOne({imdbID: data.imdbID, Year: data.Year}, callback);
}

var loadFilms = function (idFrom, idTo) {
    var id = generateImdbId(idFrom, true);
    getFilm(id, function (err, res, body) {
        var result = body.toString();
        console.log(result);
    })
};

loadFilms(4287319);

var post = function (req, res, next) {
    var d = JSON.parse(req.body.filmData);
    var uriFile = d.Poster;
    var fileName = uriFile.substr(uriFile.lastIndexOf('/') + 1);
    d.PosterOur = fileName;
    findFilm(d, function (err, film) {
        if (err) return res.send(200);
        if (film) {
            console.log(film + ' found');
            res.send(200);
        } else {
            download(d.Poster, 'public/posters/' + fileName, function () {
                console.log('done');
                res.send(200);
                console.log('200');
            });
            addFilm(d);
        }
    });
}

