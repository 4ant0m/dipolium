var New = require('../models/new').New;
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
    if (req.session.user){
        var idList = req.query.idList;
        var idFilm = req.query.idFilm;
        var subject = {
            idList: idList,
            idFilm: idFilm
        };
        req.user.removeFilm(idList,idFilm);
        New.new(req.user.nickname, subject, 'removeFilm');
        User.find({},function(err,user){
            if(err) return next(err);
            res.json(user);
        });
    }
}
