/**
 * Created by user on 1/21/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
exports.get = function(req, res, next) {
    var username = '';
    //if(req.user)
        //username = req.user.nickname;
    //else
        username = req.query.username;
    User.findOne({nickname:username}, function(err, user) {
        if (err) return next(err);
        if(user){
            console.log(user.name);
            res.render('mobile/following',{
                nickname: user.nickname,
                name: user.name,
                surname: user.surname,
                avatar :user.ava,
                about : user.about
            });
        } else {
            next (new HttpError(404, "Page not found"));
        }
    });
};
