/**
 * Created by user on 1/22/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;

exports.get = function(req, res) {
    var userID = req.query.userID;
    User.findOne({nickname: userID}, function(err, result) {
        if (err) next(err);
        if (result) {
        var user = new Object();
            user.name = result.name;
            user.surName = result.surname;
            user.ava = result.ava;
            res.send(user);
        }       
        });
    };