var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
var New = require('../models/new').New;

exports.get = function(req, res, next) {
    if (req.session.user){
        var list = new Object();
        var listcount=req.user.getListCount()+1;
        var listname = 'The List '+listcount;//req.body.listname;
        list.listname=listname;
        list.films=new Array();
        req.user.addList(list);
        //New.new(req.user.nickname, list, 'AddList');
        res.send(req.user.lists);
    } else
    {
        return next(new HttpError(401, "Вы не авторизированы"));
    }
};