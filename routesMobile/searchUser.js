/**
 * Created by user on 1/22/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;

exports.get = function(req, res) {
    var name = req.query.searchText;

    User.find({nickname: new RegExp('^'+name+'$', "i")}, function(err, users) {
        var usersArr=new Array();
        for(var i=0;i<users.length;i++){
            var user=new Object();
            user.following=0;
            for(var j=0;j<req.user.following.length;j++){
                if(req.user.following[j]==users[i].nickname){
                    user.following=1;
                    break;
                }
            }
            user.name=users[i].name;
            user.surName=users[i].surname;
            user.userName=users[i].nickname;
            if(req.user.nickname!=users[i].nickname)
            usersArr.push(user);
                      }
        res.render('mobile/searchUser',{
            searchText : name,
            users: JSON.stringify(usersArr),
            nickname: req.user.nickname,
            name: req.user.name,
            surname: req.user.surname,
            avatar : req.user.ava,
            about : req.user.about
        });
    });
};