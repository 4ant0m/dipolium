/**
 * Created by user on 26.09.14.
 */
var User = require('../models/user').User;
var Film = require('../models/film').Film;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
        var userID=req.query.userID;
        User.findOne({nickname:userID},function(err,user){
            if(err){ next(new HttpError(403,err.message));
            }else{
                if(user!=null){
                var result=user.lists;
                findFilmsRec(0);
                    function findFilmsRec(i){
                        if(i<user.lists.length){
                        Film.find({imdbID:{$in:user.lists[i].films}}, function(err,films){
                            result[i].films=films;
                            //console.log(result[i].films);
                            i++;
                            findFilmsRec(i);
                        });

                        }else{
                            res.send(result);
                        }
                    }
            }else{
                    next(new HttpError(404, 'Page not founded'));
                }
            }
        });
        //console.log(req.user.lists[0].films);

       // res.send(200);

};