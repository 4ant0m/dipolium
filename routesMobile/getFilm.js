/**
 * Created by user on 19.10.14.
 */
var Film = require('../models/film').Film;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
    var films=[];
    films = req.body.films;
    Film.find({imdbID: {$in: films}}, function(err, result){
    if(err) next(err);
    res.send(result);
});

}