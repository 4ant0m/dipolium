exports.get = function(req, res) {
    res.render('mobile/frontpage',{
        nickname: req.params.nickname,
        name: req.userFounded.name,
        surname: req.userFounded.surname,
        avatar : req.userFounded.ava,
        about : req.userFounded.about
    });
};