/**
 * Created by user on 16.08.14.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
    if(req.user){
        var index=req.query.indexList;
        req.user.removeList(index);
        res.send(req.user);
    } else{
        return next(new HttpError(401, "Вы не авторизированы"));
    }

};