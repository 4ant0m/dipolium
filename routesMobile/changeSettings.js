/**
 * Created by user on 3/19/15.
 */
var HttpError = require('../error').HttpError,
    User = require('../models/user').User,
    fs = require("fs"),
    formidable = require("formidable");

exports.post = function(req, res, next) {
        if (req.user){
            var form = new formidable.IncomingForm();
            var fileName = req.user.nickname +'.png';
            form.parse(req, function(err, fields, files) {
                if (err) next(err);
                if (files.photo !== undefined)
                fs.rename(files.photo.path, "./public/photos/"+fileName, function(err) {
                    if (err) {
                        fs.unlink("./public/photos/"+fileName);
                        fs.rename(files.photo.path, "./public/photos/"+fileName);
                    }
                    User.findOne({nickname:req.user.nickname}, function(err, user){
                        if(err) next(err);
                            if (user) {
                                user.changeAva(fileName);
                                res.render('mobile/settings',{
                                    name:req.user.name,
                                    surname:req.user.surname,
                                    mail:req.user.mail,
                                    avatar:req.user.ava,
                                    about : req.user.about
                                });
                            } else
                            next (new HttpError(401, "Page not found"));
                    })

                });

                if (fields.about !== ''){
                    req.user.changeAbout(fields.about);
                    res.render('settings',{
                        name:req.user.name,
                        surname:req.user.surname,
                        mail:req.user.mail,
                        avatar:req.user.ava,
                        about : req.user.about
                    });

                }
            });
        } else
            next (new HttpError(401, "Page not found"));
};
