/**
 * Created by user on 01.09.14.
 */
/*Поиск не совсем работает так, как мне хотелось бы, дело в том, что было бы не плохо его оптимизировать
а именно ввести возможность поиска по базе всех найденных айдишников, если они не нашлись делать на сторонний сервер запрос
*/
var Film = require('../models/film').Film;
var HttpError = require('../error').HttpError;

var request = require('request');
var api_key='4deb64f4e35f23e516cd22b0ebe545d5';
var urlOmdb= 'http://www.omdbapi.com/?s=';
//var urlIMDb='http://sg.media-imdb.com/suggests/'+name[0]+'/'+name+'.json';

var fs = require('fs'),
    request = require('request');

var download = function(uri, filename, callback){
    //i++;
    try{
        request.head(uri, function(err, res, body){
            request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
        });
    }catch (e){callback(e);
        console.log(e.message+' ');
       // if(i<10)
          //  download(uri, filename, callback,i);
    }
};


function getFilms(name,callback){request(
    {
        url: urlOmdb+name+'&plot=short',
        encoding:null,
        headers: {
            'Accept': 'application/json'
        }
    },
    callback
);}

function addFilm(data){
    Film.new(data.Title, data.Year, data.Rated, data.Released, data.Runtime, data.Genre, data.Director,data.Writer,data.Actors, data.Plot,data.Language,data.Country,data.Awards,data.Poster,data.PosterOur,data.Metascore,data.imdbRating,data.imdbVotes,data.imdbID,data.Type,data.Response);
};

function findFilm(data,callback){
    Film.findOne({imdbID:data.imdbID,Year:data.Year}, callback);

};

exports.get = function(req, res) {
    var name = req.query.searchText;
    if(name!=undefined){
            getFilms(name,function(err,responce,body){
                var result=body.toString();
                var obj=(result.substring(result.indexOf('['),result.lastIndexOf(']')+1));
                console.log(obj);
                var userNickname = '';
                if (req.user != undefined){
                    userNickname = req.user.nickname;
                }
                res.render('mobile/searchPage',{
                    arrFilms : obj,
                    nickname: userNickname,
                    searchField: req.query.searchText
                });

            });
    }else{
    res.render('registration',{
        arrFilms : '',
        nickname: userNickname,
        searchField: req.query.searchText
    });
    }
};

exports.post = function(req, res, next) {
    var d=JSON.parse(req.body.filmData);
    var uriFile= d.Poster;
    var fileName=uriFile.substr(uriFile.lastIndexOf('/')+1);
    d.PosterOur=fileName;
    findFilm(d, function(err,film) {
        if (err) return res.send(200);
        if(film){
            console.log(film+' found');
            res.send(200);
        } else {
            download(d.Poster,'public/posters/'+fileName,function(){
                console.log('done');
                res.send(200);
                console.log('200');
            });
            addFilm(d);
        }
    });

};
