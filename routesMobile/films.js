/**
 * Created by user on 16.08.14.
 */
exports.get = function(req, res) {
	var nickname = '';
	var name = '';
	var surname = '';
	var avatar = 'undefined.png'
	if (req.user) {
		avatar = req.user.ava;
    	nickname = req.user.nickname;
    	name = req.user.name;
    	surname: req.user.surname;

	}
    res.render('mobile/films',{
    	filmObject : req.filmFounded,
    	avatar : avatar,
    	nickname: nickname,
    	name: name,
    	surname: surname
    });
};