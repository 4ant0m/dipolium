/**
 * Created by user on 26.09.14.
 */
var User = require('../models/user').User;
var Film = require('../models/film').Film;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
        var userID = req.query.userID;
        var listID = req.query.listID;
        var from = req.query.from;
        var to = req.query.to;
        User.findOne({nickname:userID},function(err,user){
            if(err){next(new HttpError(403,err.message));
            }else{
                if(user !== null){
                var result = user.lists[listID];
                var arrID = [];
                for (var i = 0; i < result.films.length; i++){
                    if(result.films[i].filmID != undefined)
                        arrID.push(result.films[i].filmID);
                    else
                        arrID.push(result.films[i]);
                }               
                        Film.find({imdbID:{$in:arrID}}, function(err,films){
                            var sortedArr = [];
                            for (var t = 0 ; t < arrID.length; t++){
                                for (var j = 0; j < films.length; j++){
                                    if (arrID[t] == films[j].imdbID){
                                        sortedArr.push(films[j]);
                                        break;
                                    }
                                }
                            }
                            sortedArr.reverse();
                            result.films = sortedArr;
                            if (from !== undefined && to === undefined)
                                result.films=result.films.slice(from);
                            if (from === undefined && to !== undefined)
                                result.films=result.films.slice(0, to)
                            if (from === undefined && to === undefined)
                                result.films=result.films;
                            if (from !== undefined && to !== undefined)
                                result.films=result.films.slice(from, to)
                            res.send(result);
                        });
                        }else{
                            res.send(null);
                        }
            }
        });
};