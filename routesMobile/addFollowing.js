/**
 * Created by user on 1/15/15.
 */
var User = require('../models/user').User;
var New = require('../models/new').New;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
    if(req.session.user){
        var idUser=req.query.userID;
        req.user.addFollowing(idUser);
        New.new(req.user.nickname, idUser, 'AddFollowing');
        User.findOne({nickname:idUser}, function(err, result){
            if (err) next (err);
            result.addFollowers(req.user.nickname);
            New.new(idUser, req.user.nickname, 'AddFollowers');
            res.send(req.user.following);
        });
    } else{
        return next(new HttpError(401, "Вы не авторизированы"));
    }
};