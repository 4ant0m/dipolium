/**
 * Created by user on 23.07.14.
 */
exports.get = function(req,res){
    req.session.destroy();
    res.redirect('/');
};
