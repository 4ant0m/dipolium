/**
 * Created by user on 2/27/15.
 */
var querystring = require("querystring"),
    fs = require("fs");
    formidable = require("formidable");
var HttpError = require('../error').HttpError;

exports.post=function(req, res, next) {
    if(req.session.user){
    var form = new formidable.IncomingForm();
    form.parse(req, function(error, fields, files) {
        fs.rename(files.upload.path, "./public/ava"+req.user.nickname+".png", function(err) {
            if (err) {
                fs.unlink("./public/ava"+req.user.nickname+".png");
                fs.rename(files.upload.path, "./public/ava"+req.user.nickname+".png");
            }
        });
        res.send(200);
        res.end();
    });
    }else{
        return next(new HttpError(401, "Вы не авторизированы"));
    }
}
