var Film = require('../models/film').Film;
var User = require('../models/user').User;
var checkAuth = require('../middleware/checkAuth');
var findUser = require('../middleware/findUser');
var findFilm = require('../middleware/findFilm');

module.exports = function(router) {

try{
    router.get('/addList', require('./addList').get);
    router.get('/addFilm', require('./addFilm').get);
    router.get('/removeList', require('./removeList').get);
    router.get('/removeFilm', require('./removeFilm').get);
    router.get('/login', require('./login').get);
    router.post('/login', require('./login').post);
    router.get('/logout', require('./logout').get);
    router.get('/getFilm',require('./getFilm').get);
    router.get('/allList',require('./allList').get);

    router.get('/id_:nickname', findUser, require('./frontpage').get);
    router.get('/films/:idFilm', findFilm, require('./films').get);
    router.get('/getUsers', function(request,response,next){
        User.find({},function(err,films){
            if(err) return next(err);
            response.json(films);
        });
    });
    router.get('/findFilm', require('./findFilm').get);

    router.get('/searchPage', require('./searchPage').get);
    router.get('/searchUser', require('./searchUser').get);
    router.get('/getListFilms', require('./getListFilms').get);
    router.get('/getLists', require('./getLists').get);
    router.get('/getListsName', require('./getListsName').get);
    router.get('/getListName', require('./getListName').get);
    router.get('/getFilmName', require('./getFilmName').get);
    router.get('/getCountFollowers', require('./getCountFollowers').get);
    router.get('/getCountFollowing', require('./getCountFollowing').get);
    router.get('/getYourRecords', require('./getYourRecords').get);
    router.get('/getComment', require('./getComment').get);
    router.get('/getUserInfo', require('./getUserInfo').get);

    router.get('/followers',require('./followers').get);
    router.get('/following',require('./following').get);
    router.post('/searchPage', require('./searchPage').post);
    router.get('/settings', require('./settings').get);
    router.post('/updateImage', require('./updateImage').post);
    router.get('/getNews', require('./getNews').get);
    router.get('/getAllNews', require('./getAllNews').get);
    router.get('/renameList', require('./renameList').get);
    router.get('/removeAllNews', require('./removeAllNews').get);
    router.get('/getFollowing', require('./getFollowing').get);
    router.get('/getFollowers', require('./getFollowers').get);
    router.get('/addFollowing', require('./addFollowing').get);
    router.get('/removeFollowing', require('./removeFollowing').get);
    router.post('/changeSettings', require('./changeSettings').post);
    router.get('/registration', require('./registration').get);
    router.get('/about', require('./about').get)

    router.get('/:other?', checkAuth, function(req,res,next){
        res.redirect('/id_'+req.user.nickname);
    });
}catch(e){
    console.log(e.message);
}
};
