/**
 * Created by user on 3/4/15.
 */
var User = require('../models/user').User,
    New = require('../models/new').New,
    Film = require('../models/film').Film,
    HttpError = require('../error').HttpError;

/*exports.get = function(req, res, next) {
        var idUser = req.query.userID;
        var from = req.query.from;
        var to = req.query.to;
        User.findOne({nickname : idUser}, function (err, user) {
            if (err) next (err);
            if (user)
            New.find().sort({"Date" : -1}).exec(function (err, result) {
                if (err) next (err);
                var resultFiltered =  new Array();
                for (var i = 0; i < result.length; i++){
                    for (var j = 0; j < user.following.length; j++){
                        if (result[i].Object === user.following[j]){
                            if (result[i].Info === "AddFollowing" || result[i].Info === "AddFilm" )
                            resultFiltered.push(result[i]);
                            break;
                        }
                    }
                }
                if (from !== undefined && to === undefined)
                     res.send(resultFiltered.slice(from));
                if (from === undefined && to !== undefined)
                     res.send(resultFiltered.slice(0, to));
                if (from === undefined && to === undefined)
                     res.send(resultFiltered);
                if (from !== undefined && to !== undefined)
                     res.send(resultFiltered.slice(from, to));
            });
            else next (new HttpError(404, "Page not found"));
        });
}*/
exports.get = function(req, res, next) {
    var idUser = req.query.userID;
    var from = req.query.from || 0;
    var to = req.query.to || from + 10;
    var dif = to - from;
    User.findOne({nickname : idUser}, function (err, user) {
        //этот кусок кода нужно переписать, из-за того что динамически формируется массив, на данном этапе, что бы исправить, нужно переписывать большую часть кода
        var arr = [];
        for (var i = 0; i < user.following.length; i++){
            if (user.following[i].id == undefined)
                arr.push(user.following[i]);
            else
                arr.push(user.following[i].id);
        }
        if (err) next (err);
        if (user)
            New.find().skip(from).limit(dif).sort({'Date' : -1}).where('Info').in(['AddFollowing', 'AddFilm']).where('Object').in(arr).exec(function (err, result) {
                if (err) next (err);
				res.send(result);
            });
        else next (new HttpError(404, "Page not found"));
    });

    function getAboutFilm (result, i, callback){

        for (var i = i; i < result.length; i++){
            if (result[i].Info === "AddFilm") {
            User.findOne({nickname : result[i].Object}, function (err, user) {
            Film.findOne({imdbID:result[i].Subject.idFilm}, function(err, film){
                var idFilm = result[i].Subject.idFilm;
                if (err) {next(new HttpError(403, err.message));
                } else {
                	
                	//if(result[i].Subject.idList == 7) console.log(result[i])
                    if (user.lists.length > result[i].Subject.idList) {
                        result[i].Subject = {
                            list : user.lists[result[i].Subject.idList].listname,
                            idList : result[i].Subject.idList
                        }
                        if (film !== null) {
                            var comment = '';
                            for (var j = 0; j < user.lists[result[i].Subject.idList].films.length; j++){
                                if (idFilm == user.lists[result[i].Subject.idList].films[j].filmID){
                                    comment = user.lists[result[i].Subject.idList].films[j].comment
                                    break;
                                }
                            }
                            result[i].Subject.film = {
                                title : film.Title,
                                id : film.imdbID,
                                poster : film.PosterOur,
                                country : film.Country,
                                year : film.Year,
                                comment : comment
                            }
                        }
                    } else{
                    	
                        result.splice(i,1);
                        i--;
                       // console.log(i, ' ',result.length);
                    }
                    
                }
                console.log(i, ' ',result.length);
                if (i == result.length-3){

                     return callback(result);
                }else
                return getAboutFilm(result, i+1, callback);
            });
        });
                break;
        }

    }
    }
}