/**
 * Created by user on 17.08.14.
 */
var User = require('../models/user').User;
var New = require('../models/new').New;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
    if(req.session.user){
        var idList=req.query.idList;
        var idFilm=req.query.idFilm;
        var subject = {
            idList :idList,
            idFilm :idFilm
        }
        req.user.addFilm(idList, idFilm);
        New.new(req.user.nickname, subject, 'AddFilm');
        res.send(200);
    };
}