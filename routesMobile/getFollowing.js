/**
 * Created by user on 3/7/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
exports.get = function(req, res, next) {
    var userID = req.query.userID;
    var from = req.query.from;
    var to = req.query.to;
    User.findOne({nickname: userID}, function(err, result) {
        if (err) return next(err);
        if (result){
            var usersArr = new Array();
            var followingArr = new Array();
            for(var k = 0 ; k < result.following.length; k++){
                if(result.following[k].id != undefined)
                    followingArr.push(result.following[k].id);
                else
                    followingArr.push(result.following[k]);
            }
                User.find({nickname: {$in: followingArr}}, function (err, users) {
                    if (err) return next(err);
                    for (var i = 0; i < users.length; i++){
                        for (var j = 0; j < followingArr.length; j++)
                             if (users[i].nickname ===  followingArr[j]) {
                                 var user = new Object();
                                 user.following = 1;
                                 user.name = users[i].name;
                                 user.surName = users[i].surname;
                                 user.userName = users[i].nickname;
                                 user.avatar = users[i].ava;
                                 if (result.nickname !== users[i].nickname)
                                     usersArr.push(user);
                             }
                    }
                    if (from !== undefined && to === undefined)
                        res.send(usersArr.slice(from));
                    if (from === undefined && to !== undefined)
                        res.send(usersArr.slice(0, to));
                    if (from === undefined && to === undefined)
                        res.send(usersArr);
                    if (from !== undefined && to !== undefined)
                        res.send(usersArr.slice(from, to));
                });
        } else {
            next (new HttpError(404, "Page not found"));
        }
    });
};
