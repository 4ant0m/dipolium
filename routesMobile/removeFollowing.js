/**
 * Created by user on 1/21/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
var New = require('../models/new').New;

exports.get = function(req, res, next) {
    if (req.session.user) {
        var idUser = req.query.userID;
        req.user.removeFollowing(idUser);
        New.new(req.user.nickname, idUser, 'unFollowing');
        User.findOne({nickname : idUser}, function (err, result){
            if(err) next(err);
            result.removeFollowers(req.user.nickname);
            New.new(idUser, req.user.nickname, 'removeFollowers');
            res.send(req.user.following);
        });
    } else{
        return next(new HttpError(401, "Вы не авторизированы"));
    }
};