/**
 * Created by user on 3/23/15.
 */
var User = require('../models/user').User,
    Film = require('../models/film').Film,
    HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
        var idFilm = req.query.idFilm;
    Film.findOne({imdbID:idFilm}, function(err, film){
        if (err) {next(new HttpError(403, err.message));
        } else {
            if (film !== null) {
                    res.send(film.Title);
            }else{
                res.send(null);
            }
        }
    });
}