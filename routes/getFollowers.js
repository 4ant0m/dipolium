/**
 * Created by user on 3/7/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
exports.get = function(req, res, next) {
    var userID = req.query.userID;
    var from = req.query.from;
    var to = req.query.to;
    User.findOne({nickname: userID}, function(err, result) {
        if (err) return next (err);
            var usersArr = new Array();
            User.find({nickname: {$in: result.followers}}, function (err, users) {
                if (err) return next(err);
                for (var i = 0; i < users.length; i++){
                    var user = new Object();
                    user.following = 0;
                    for (var j = 0; j < result.following.length; j++)
                        if (users[i].nickname === result.following[j] || users[i].nickname ===  result.following[j].id) {
                            user.following = 1;
                        }
                    user.name = users[i].name;
                    user.surName = users[i].surname;
                    user.userName = users[i].nickname;
                    user.avatar = users[i].ava;
                    if (result.nickname !== users[i].nickname)
                        usersArr.push(user);
                }
                if (from !== undefined && to === undefined)
                    res.send(usersArr.slice(from));
                if (from === undefined && to !== undefined)
                    res.send(usersArr.slice(0, to));
                if (from === undefined && to === undefined)
                    res.send(usersArr);
                if (from !== undefined && to !== undefined)
                    res.send(usersArr.slice(from, to));
            });
    });
};