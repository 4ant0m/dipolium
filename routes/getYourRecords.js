/**
 * Created by user on 26.09.14.
 */
var User = require('../models/user').User;
var Film = require('../models/film').Film;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
        var userID = req.query.userID;
        var from = req.query.from;
        var to = req.query.to;
        User.findOne({nickname: userID}, function(err,user){
            if(err){next(new HttpError(403,err.message));
            }else{
                if(user !== null){
                    var resultFiltered = new Array();
                        resultFiltered = user.records.slice();
                        resultFiltered.reverse();
                    if (from !== undefined && to === undefined)
                        res.send(resultFiltered.slice(from));
                    if (from === undefined && to !== undefined)
                        res.send(resultFiltered.slice(0, to));
                    if (from === undefined && to === undefined)
                        res.send(resultFiltered);
                    if (from !== undefined && to !== undefined)
                        res.send(resultFiltered.slice(from, to));
                }
            }
               
        });
};