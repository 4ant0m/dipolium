/**
 * Created by user on 2/18/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
exports.get = function(req, res, next) {
    var username = '';
    if (req.user)
        username = req.user.nickname;
    else
        username = req.query.username;
    User.findOne({nickname:username}, function(err, user) {
        if (err) return next (err);
        if(user){
            res.render('settings',{
                nickname: user.nickname,
                name: user.name,
                surname: user.surname,
                mail: user.mail,
                avatar: user.ava,
                about: user.about
            });
        } else
            next (new HttpError(401, "Page not found"));
    });
};
