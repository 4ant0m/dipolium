/**
 * Created by user on 17.08.14.
 */
var Datastore = require('nedb')
    , db = {};

db.emails = new Datastore({filename: './emails/emails.json'});

db.emails.loadDatabase(function (err) {
    if (err) return err;
});

exports.post = function(req, res, next) {
    var email = {
        email : req.body.email
    };
    db.emails.insert(email, function (err, newDoc) {
        if (err) return next(err);
        return res.send(newDoc);
    });
};