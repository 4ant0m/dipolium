/**
 * Created by user on 23.10.14.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;

exports.get = function(req, res) {
    var username = req.query.username;
    var listID = req.query.listID;
    if (listID == undefined) listID = 0;
    User.findOne({nickname:username}, function(err,user) {
        if (err) return next(err);
        if(user){
            res.render('allList',{
                nickname: user.nickname,
                name: user.name,
                surname: user.surname,
                avatar : user.ava,
                about : user.about,
                listID : listID
              
            });
        } else {
            next(new HttpError(404, "Page not found"));
        }

});
}