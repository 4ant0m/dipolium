/**
 * Created by user on 26.09.14.
 */
var User = require('../models/user').User;
var Film = require('../models/film').Film;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
        var userID = req.query.userID;
        var listID = req.query.listID;
        var filmID = req.query.filmID;
        User.findOne({nickname: userID}, function(err,user){
            if(err){next(new HttpError(403,err.message));
            }else{
                if(user !== null){
                var result = user.lists[listID].films;
                var likes = [];
                for (var i = 0; i < result.length; i++){
                    if (result[i].filmID == filmID){
                        if (result[i].likes == undefined)
                            likes = [];
                        else
                            likes = result[i].likes;
                        break;
                    }
                }
                console.log(likes);
                res.send(likes);
                        } else {
                            res.send(null);
                        }
            }
        });
};