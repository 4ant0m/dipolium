var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
var AuthError = require('../models/user').AuthError;
var async = require('async');

exports.get = function(req, res) {
    res.render('login');
};

exports.post = function(req, res, next) {
    var name = req.body.name;
    var surname = req.body.surname;
    var nickname = req.body.nickname;
    var password = req.body.password;
    var email = req.body.email;
    User.authorize(email, name, surname, password, nickname, function(err, user) {
        if (err) {
            if (err instanceof AuthError) {
                return res.send(new HttpError(403, err.message));
            } else {
                return res.send(err);
            }
        }else{
            req.session.user = user._id;
            console.log(user.nickname);
            res.send(200);
    }

    });

};
