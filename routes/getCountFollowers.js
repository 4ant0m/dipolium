/**
 * Created by user on 3/7/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
exports.get = function(req, res, next) {
    var userID = req.query.userID;
    User.findOne({nickname: userID}, function(err, result) {
        if (err) return next(err);
        if (result.followers) {
           res.send(result.followers.length+'');
        } else {
            next (new HttpError(404, "Page not found"));
        }
    });
};