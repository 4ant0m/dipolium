/**
 * Created by user on 16.08.14.
 */
exports.get = function(req, res) {
	var nickname = '';
	var avatar = 'undefined.png'
	if (req.user) {
		avatar = req.user.ava;
    	nickname = req.user.nickname;
	}
    res.render('films',{
    	filmObject : req.filmFounded,
    	avatar : avatar,
    	nickname: nickname
    });
};