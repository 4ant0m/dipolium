/**
 * Created by user on 26.09.14.
 */
var User = require('../models/user').User;
var Film = require('../models/film').Film;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
        var userID = req.query.userID;
        var listID = req.query.listID;
        User.findOne({nickname:userID},function(err,user){
            if(err){next(new HttpError(403,err.message));
            }else{
                if(user!=null){
                var result = user.lists[listID].films.length;
                    if(result)
                        res.send(result+'');
                    else
                        res.send(0+'');
                        }else{
                            res.send(null);
                        }
            }
        });
};