
var User = require('../models/user').User,
    HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
	var from = req.query.from;
    var to = req.query.to;
User.find().sort({"count_followers" : -1}).exec( function(err, resultFiltered) {
    if (err) {next(new HttpError(403,err.message));
    } else {
        if (resultFiltered !== null){
                 if (from !== undefined && to === undefined)
                        res.send(resultFiltered.slice(from));
                    if (from === undefined && to !== undefined)
                        res.send(resultFiltered.slice(0, to));
                    if (from === undefined && to === undefined)
                        res.send(resultFiltered);
                    if (from !== undefined && to !== undefined)
                        res.send(resultFiltered.slice(from, to));
        }else{
            res.send('false');
        }
    }
});
}