exports.get = function(req, res) {
    res.render('frontpage',{
        nickname: req.params.nickname,
        name: req.userFounded.name,
        surname: req.userFounded.surname,
        avatar : req.userFounded.ava,
        about : req.userFounded.about
    });
};