/**
 * Created by user on 27.10.14.
 */
/**
 * Created by user on 26.09.14.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
    var userID=req.query.userID;
    User.findOne({nickname:userID},function(err,user){
        if(err){next(new HttpError(403,err.message));
        }else{
            if(user!=null){
                var result=[];
                for(var i=0;i<user.lists.length;i++){
                    result.push(user.lists[i].listname);
                }
                console.log(result);
                res.send(result);
            }else{
                res.send(null);
            }
        }
    });
};