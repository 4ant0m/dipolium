/**
 * Created by user on 1/22/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;

exports.get = function(req, res) {
    var name = req.query.searchText;

    User.find({nickname: new RegExp('^'+name+'$', "i")}, function(err, users) {
        var usersArr = new Array();
        for (var i = 0; i < users.length;i ++){
            var user = new Object();
            if (req.user)
            var userNickname = req.user.nickname;
            user.following = 0;
            if(req.user != undefined)
            for(var j = 0; j < req.user.following.length; j++){
                if(req.user.following[j] == users[i].nickname){
                    user.following = 1;
                    break;
                }
            }
            user.name = users[i].name;
            user.surName = users[i].surname;
            user.userName = users[i].nickname;
            
            if (userNickname!= users[i].nickname)
                usersArr.push(user);
        }
        res.render('searchUser',{
            searchText : name,
            users: JSON.stringify(usersArr)
        });
    });
};