/**
 * Created by user on 16.08.14.
 */
var New = require('../models/new').New;
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
    if (req.user){
        var index = req.query.indexList;
         New.find().where('Object').equals(req.user.nickname).where('Subject.idList').equals(index).remove().exec(function(err, result){
            if (err) console.log(err);
           });
        req.user.removeList(index);
        res.send(200);
    } else{
        return next(new HttpError(401, "You are not logged in"));
    }

};