/**
 * Created by user on 17.08.14.
 */
var User = require('../models/user').User;
var New = require('../models/new').New;
var HttpError = require('../error').HttpError;

exports.post = function(req, res, next) {
    if(req.session.user){
        var idList = req.body.idList;
        var idFilm = req.body.idFilm;
        var comment = req.body.comment;
        var subject = {
            idList :idList,
            idFilm :idFilm,
            comment :comment
        }
        req.user.addFilm(idList, idFilm, comment);
        New.find().where('Object').equals(req.user.nickname).where('Subject.idList').equals(subject.idList).where('Subject.idFilm').equals(subject.idFilm).remove().exec(function(err){
            if (err) console.log(err);
           });
        New.new(req.user.nickname, subject, 'AddFilm');
        res.send(200);
    };
}