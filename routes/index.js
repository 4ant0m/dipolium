var Film = require('../models/film').Film;
var User = require('../models/user').User;
var checkAuth = require('../middleware/checkAuth');
var findUser = require('../middleware/findUser');
var findFilm = require('../middleware/findFilm');

module.exports = function(app) {
   // app.get('/',checkAuth, require('./page').get);
    app.get('/addList', require('./addList').get);
    app.post('/addFilm', require('./addFilm').post);
    app.get('/addLike', require('./addLike').get);
    app.get('/removeList', require('./removeList').get);
    app.get('/removeFilm', require('./removeFilm').get);
    app.get('/login', require('./login').get);
    app.post('/login', require('./login').post);
    app.get('/logout', require('./logout').get);
    app.post('/getFilm',require('./getFilm').post);
    app.get('/allList',require('./allList').get);

    app.get('/id_:nickname', findUser, require('./frontpage').get);
    app.get('/films/:idFilm', findFilm, require('./films').get);
    app.get('/getUsers', function(request,response,next){
        User.find({},function(err,films){
            if(err) return next(err);
            response.json(films);
        });
    });
    app.get('/getFilms', function(request,response,next){
        Film.find({},function(err,films){
            if(err) return next(err);
            response.json(films);
        });
    });
    app.get('/findFilm', require('./findFilm').get);

    app.get('/searchPage', require('./searchPage').get);
    app.get('/searchUser', require('./searchUser').get);
    app.get('/getListFilms', require('./getListFilms').get);
    app.get('/getLists', require('./getLists').get);
    app.get('/getListsName', require('./getListsName').get);
    app.get('/getListName', require('./getListName').get);
    app.get('/getFilmName', require('./getFilmName').get);
    app.get('/getCountFilmsInList', require('./getCountFilmsInList').get);
    app.get('/getCountFollowers', require('./getCountFollowers').get);
    app.get('/getCountFollowing', require('./getCountFollowing').get);
    app.get('/getComment', require('./getComment').get);
    app.get('/getUserInfo', require('./getUserInfo').get);
    app.get('/getLikes', require('./getLikes').get);

    app.get('/checkNickname', require('./checkNickname').get);
    app.get('/checkEmail', require('./checkEmail').get);
    app.get('/getMostPopularUsers', require('./getMostPopularUsers').get);

    app.get('/followers',require('./followers').get);
    app.get('/following',require('./following').get);
    app.post('/searchPage', require('./searchPage').post);
    app.get('/settings', require('./settings').get);
    app.post('/updateImage', require('./updateImage').post);
    app.get('/getNews', require('./getNews').get);
    app.get('/getYourRecords', require('./getYourRecords').get);
    app.get('/getAllNews', require('./getAllNews').get);
    app.get('/renameList', require('./renameList').get);
    app.get('/removeAllNews', require('./removeAllNews').get);
    app.get('/getFollowing', require('./getFollowing').get);
    app.get('/getFollowers', require('./getFollowers').get);
    app.get('/addFollowing', require('./addFollowing').get);
    app.get('/removeFollowing', require('./removeFollowing').get);
    app.post('/changeSettings', require('./changeSettings').post);
    app.get('/registration', require('./registration').get);
    app.get('/about', require('./about').get);
    app.get('/users', require('./users').get);
    app.get('/movies', require('./movies').get);
    app.get('/', require('./landing-v2').get);
    app.get('/saveEmail', require('./saveEmail').post);


    app.get('/:other?', checkAuth, function(req,res,next){
        res.redirect('/id_'+req.user.nickname);
    });
};



/*var User=require('../models/user').User;
var HttpError = require('../error').HttpError;
var ObjectID = require('mongodb').ObjectID;

module.exports = function(app){
    app.get('/', function(request,response,next){
        response.render('index');
    });

    app.get('/users', function(request,response,next){
        User.find({},function(err,users){
            if(err) return next(err);
            response.json(users);
        });
    });
    app.get('/users/:id', function(request,response,next){
        try{
        var id= new ObjectID(request.params.id);
        }catch (e){
            return next(404);
        }
        User.findById(id,function(err,user){
            if (err) return next(err);
            if(!user){
                next(new HttpError(404,"User not found"))
            }
            response.json(user);
        });
    });
};*/
