/**
 * Created by user on 3/5/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
var New = require('../models/new').New;

exports.get = function(req, res, next) {
    if (req.session.user){
        var index = req.query.indexList;
        var title = req.query.title;
        req.user.renameList(index, title);
        res.send(200);
    } else
    {
        return next(new HttpError(401, "Вы не авторизированы"));
    }
};