/**
 * Created by user on 3/19/15.
 */
var HttpError = require('../error').HttpError,
    User = require('../models/user').User,
    fs = require("fs"),
    formidable = require("formidable");
    path = require("path");

exports.post = function(req, res, next) {
        if (req.user) {
            var form = new formidable.IncomingForm();
            var fileName = req.user.nickname +'.png';
            form.parse(req, function(err, fields, files) {
                if (err) next (err);
                console.log(files)
                if (files.photo!= undefined)
                    if (files.photo.size > 0)
                        fs.rename(files.photo.path,  path.resolve('.')+"/public/photos/"+fileName, function(err) {
                            if (err) {
                                fs.unlink(path.resolve('.')+"/public/photos/"+fileName);
                                fs.rename(files.photo.path, path.resolve('.')+"/public/photos/"+fileName);
                            }
                            User.findOne({nickname:req.user.nickname}, function(err, user){
                                if(err) next(err);
                                    if (user) {
                                        user.changeAva(fileName);
                                    } else
                                    next (new HttpError(401, "Page not found"));
                            })

                        });
                if (files.name != '' && fields.name != undefined)
                    req.user.changeName(fields.name);
                if (files.surname != '' && fields.surname != undefined)
                    req.user.changeSurname(fields.surname);
                if (fields.email != '' && fields.email != undefined)
                    req.user.changeMail(fields.email);
                if (fields.pass1 != '' && fields.pass2 != '' && fields.pass3 != '' && fields.pass1 != undefined && fields.pass2 != undefined && fields.pass3 != undefined)
                   req.user.changePassword(fields.pass1, fields.pass2, fields.pass3);
                if (fields.about != '' && fields.about != undefined)
                   req.user.changeAbout(fields.about);
                    res.render('settings',{
                        nickname: req.user.nickname,
                        name:req.user.name,
                        surname:req.user.surname,
                        mail:req.user.mail,
                        avatar:req.user.ava,
                        about : req.user.about
                    });
            });
        } else
            next (new HttpError(401, "Page not found"));
};
