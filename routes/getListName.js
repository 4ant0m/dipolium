/**
 * Created by user on 3/23/15.
 */
var User = require('../models/user').User,
    HttpError = require('../error').HttpError;

exports.get = function(req, res, next) {
var userID = req.query.userID,
    idList = req.query.idList;

User.findOne({nickname:userID}, function(err, user){
    if (err) {next(new HttpError(403,err.message));
    } else {
        if (user !== null){
            if (user.lists.length > idList)
                res.send(user.lists[idList].listname);
            else
                res.send(null);
        }else{
            res.send(null);
        }
    }
});
}