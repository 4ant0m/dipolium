/**
 * Created by user on 1/21/15.
 */
var User = require('../models/user').User;
var HttpError = require('../error').HttpError;
var New = require('../models/new').New;

exports.get = function(req, res, next) {
    if (req.session.user) {
        var idUser = req.query.userID;
        req.user.removeFollowing(idUser);
         New.find().where('Object').equals(req.user.nickname).where('Subject').equals(idUser).remove().exec(function(err){
            if (err) console.log(err);
           });
        User.findOne({nickname : idUser}, function (err, result){
            if(err) next(err);
            result.removeFollowers(req.user.nickname);
         New.find().where('Object').equals(idUser).where('Subject').equals(req.user.nickname).remove().exec(function(err){
            if (err) console.log(err);
           });
            res.send(req.user.following);
        });
    } else{
        return next(new HttpError(401, "You are not loggin"));
    }
};